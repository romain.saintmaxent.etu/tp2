import Component from "./components/Component.js";


const title = new Component( 'h1', 'La carte' );
document.querySelector('.pageTitle').innerHTML = title.render();
const img = new Component( 'img' );
document.querySelector( '.pageContent' ).innerHTML = img.render();
console.log(img.render())